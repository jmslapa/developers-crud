<?php


namespace Modules\Developers\ViewModels;


use Illuminate\Http\JsonResponse;
use Support\Contracts\UI\ViewModel;

class RestResponseViewModel implements ViewModel
{

    private JsonResponse $response;

    public function __construct(JsonResponse $jsonResponse)
    {
        $this->response = $jsonResponse;
    }

    public function getResponse(): JsonResponse
    {
        return $this->response;
    }

}