<?php

use Illuminate\Support\Facades\Route;
use Modules\Developers\Http\Controllers\Rest\CreateDeveloperRestController;
use Modules\Developers\Http\Controllers\Rest\DeleteDeveloperRestController;
use Modules\Developers\Http\Controllers\Rest\FindDeveloperByIdRestController;
use Modules\Developers\Http\Controllers\Rest\ListDevelopersRestController;
use Modules\Developers\Http\Controllers\Rest\ReplaceDeveloperRestController;

Route::middleware(['api'])->prefix('api/developers')->name('api.developers.')->group(function () {

    Route::get('/', ListDevelopersRestController::class)->name('list');
    Route::post('/', CreateDeveloperRestController::class)->name('create');

    Route::get('/{id}', FindDeveloperByIdRestController::class)->name('findById');
    Route::put('/{id}', ReplaceDeveloperRestController::class)->name('replace');
    Route::delete('/{id}', DeleteDeveloperRestController::class)->name('delete');
});