<?php


namespace Modules\Developers\FiltersMaps;


use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Support\Database\Builder as CustomBuilder;
use Support\Contracts\Domain\FiltersMap;

class DeveloperFiltersMap implements FiltersMap
{

    private null|int $id;
    private null|string $nome;
    private null|string $sexo;
    private null|array|int $idade;
    private null|string $hobby;
    private null|array|string $datanascimento;

    private function __construct()
    {
        $this->id             = null;
        $this->nome           = null;
        $this->sexo           = null;
        $this->idade          = null;
        $this->hobby          = null;
        $this->datanascimento = null;
    }

    public static function build(): DeveloperFiltersMap
    {
        return new DeveloperFiltersMap();
    }

    function apply(EloquentBuilder|QueryBuilder $query): QueryBuilder|EloquentBuilder|CustomBuilder
    {
        return $query->when($this->id, fn(EloquentBuilder|QueryBuilder $q) => $q->where('id', $this->id))
                     ->when($this->sexo, fn(EloquentBuilder|QueryBuilder $q) => $q->where('sexo', $this->sexo))
                     ->when($this->nome,
                         fn(EloquentBuilder|QueryBuilder $q) => $q->where('nome', 'like', "%$this->nome%"))
                     ->when($this->hobby,
                         fn(EloquentBuilder|QueryBuilder $q) => $q->where('hobby', 'like', "%$this->hobby%"))
                     ->when($this->idade,
                         fn(EloquentBuilder|QueryBuilder $q) => $this->applyEqualOrBetween($q, 'idade', $this->idade))
                     ->when($this->datanascimento,
                         fn(EloquentBuilder|QueryBuilder $q) => $this->applyEqualOrBetween($q, 'datanascimento',
                             $this->datanascimento));
    }

    protected function applyEqualOrBetween(
        EloquentBuilder|QueryBuilder $query,
        string $field,
        $argument
    ): EloquentBuilder|QueryBuilder {
        if (is_array($argument) && count($argument) >= 2) {
            [$start, $end] = $argument;

            if ($start && $end) {
                $query->whereBetween($field, $argument);
            } elseif ($start) {
                $query->where($field,  '>=', $start);
            } elseif ($end) {
                $query->where($field, '<=', $end);
            }
        } elseif ( ! is_array($argument)) {
            $query->where($field, $argument);
        }

        return $query;
    }

    public function id(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function nome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function sexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function idade(array|int $idade): self
    {
        $this->idade = $idade;

        return $this;
    }

    public function hobby(string $hobby): self
    {
        $this->hobby = $hobby;

        return $this;
    }

    public function datanascimento(array|string $datanascimento): self
    {
        $this->datanascimento = $datanascimento;

        return $this;
    }

}