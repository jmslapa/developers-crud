<?php

namespace Modules\Developers\Models;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Developers\Factories\DeveloperFactory;
use Modules\Developers\FiltersMaps\DeveloperFiltersMap;
use Support\Abstracts\Domain\BaseModel;

/**
 * Class Developer
 *
 * @package Modules\Developers\Models
 *
 * @property int $id
 * @property string $nome
 * @property string $sexo
 * @property int $idade
 * @property string $hobby
 * @property string $datanascimento
 * @property string $created_at
 * @property string $updated_at
 *
 * @method static Factory|DeveloperFactory factory()
 */
class Developer extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'sexo',
        'idade',
        'hobby',
        'datanascimento',
    ];

    protected static function newFactory(): Factory|DeveloperFactory
    {
        return app(DeveloperFactory::class);
    }

    public static function filters(): DeveloperFiltersMap
    {
        return DeveloperFiltersMap::build();
    }
}
