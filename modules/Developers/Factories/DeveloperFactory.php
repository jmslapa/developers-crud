<?php

namespace Modules\Developers\Factories;

use Illuminate\Database\Eloquent\Collection as Collection;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Modules\Developers\Models\Developer;

class DeveloperFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Developer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gender = Arr::random(['F', 'M']);

        return [
            'id'             => null,
            'nome'           => $this->withFaker()->name($gender === 'F' ? 'female' : 'male'),
            'sexo'           => $gender,
            'idade'          => $age = $this->faker->numberBetween(16, 60),
            'hobby'          => $this->faker->realText(),
            'datanascimento' => $this->faker->dateTimeBetween("-$age years", "-$age years")->format('Y-m-d'),
        ];
    }

    /**
     * @param  array  $attributes
     * @param  \Illuminate\Database\Eloquent\Model|null  $parent
     *
     * @return Collection|Model|Developer
     */
    public function create($attributes = [], ?Model $parent = null)
    {
        return parent::create($attributes, $parent);
    }

    /**
     * @param  array  $attributes
     * @param  \Illuminate\Database\Eloquent\Model|null  $parent
     *
     * @return Collection|Model|Developer
     */
    public function make($attributes = [], ?Model $parent = null)
    {
        return parent::make($attributes, $parent);
    }

}
