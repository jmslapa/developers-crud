<?php


namespace Modules\Developers\Services;


use Support\Contracts\UI\ViewModel;

interface FindDeveloperByIdInputPort
{
    public function findDeveloperById(int $id): ViewModel;
}