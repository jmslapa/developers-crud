<?php


namespace Modules\Developers\Services;


use Modules\Developers\RequestModels\CreateDeveloperRequestModel;
use Support\Contracts\UI\ViewModel;

interface CreateDeveloperInputPort
{
    public function createDeveloper(CreateDeveloperRequestModel $requestModel): ViewModel;
}