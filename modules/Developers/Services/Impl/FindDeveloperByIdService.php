<?php


namespace Modules\Developers\Services\Impl;


use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Presenters\FindDeveloperByIdOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Support\ResponseModels\NotFoundRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use Modules\Developers\Services\FindDeveloperByIdInputPort;
use Support\Contracts\UI\ViewModel;
use Throwable;

class FindDeveloperByIdService implements FindDeveloperByIdInputPort
{

    private DeveloperRepository $developerRepository;
    private FindDeveloperByIdOutputPort $outputPort;

    public function __construct(DeveloperRepository $developerRepository, FindDeveloperByIdOutputPort $outputPort)
    {
        $this->developerRepository = $developerRepository;
        $this->outputPort          = $outputPort;
    }


    public function findDeveloperById(int $id): ViewModel
    {
        try {
            $developer = $this->developerRepository->findById($id);
            if ( ! $developer) {
                throw new DeveloperNotFoundException('Developer not found.');
            }

            return $this->outputPort->developerRetrieved(new DeveloperResource($developer));
        } catch (DeveloperNotFoundException $e) {
            return $this->outputPort->developerNotFound(new NotFoundRestResponseModel($e->getMessage()), $e);
        } catch (Throwable $th) {
            return $this->outputPort->unableToRetrieveDeveloper(
                new UnexpectedErrorRestResponseModel(
                    'An error occurred during developer retrieving. Please, contact the support team.'
                ),
                $th
            );
        }
    }

}