<?php


namespace Modules\Developers\Services\Impl;


use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Presenters\DeleteDeveloperOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\Services\DeleteDeveloperInputPort;
use Support\Contracts\UI\ViewModel;
use Support\ResponseModels\BadRequestRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use Throwable;

class DeleteDeveloperService implements DeleteDeveloperInputPort
{

    private DeveloperRepository $developerRepository;
    private DeleteDeveloperOutputPort $outputPort;

    public function __construct(DeveloperRepository $developerRepository, DeleteDeveloperOutputPort $outputPort)
    {
        $this->developerRepository = $developerRepository;
        $this->outputPort          = $outputPort;
    }

    public function deleteDeveloper(int $id): ViewModel
    {
        try {
            $developer = $this->developerRepository->findById($id) ?? throw new DeveloperNotFoundException(
                    'Developer not found.'
                );
            $this->developerRepository->delete($developer);

            return $this->outputPort->developerDeleted();
        } catch (DeveloperNotFoundException $e) {
            return $this->outputPort->developerDoesNotExists(
                new BadRequestRestResponseModel('Developer not exists.', []),
                $e
            );
        } catch (Throwable $th) {
            return $this->outputPort->unableToDeleteDeveloper(
                new UnexpectedErrorRestResponseModel(
                    'There was an error during developer deleting. Please, contact the support team.'
                ),
                $th
            );
        }
    }

}