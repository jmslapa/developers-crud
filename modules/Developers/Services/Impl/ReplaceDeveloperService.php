<?php


namespace Modules\Developers\Services\Impl;


use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Presenters\ReplaceDeveloperOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\RequestModels\ReplaceDeveloperRequestModel;
use Modules\Developers\Services\ReplaceDeveloperInputPort;
use Support\Contracts\UI\ViewModel;
use Support\ResponseModels\BadRequestRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use Throwable;

class ReplaceDeveloperService implements ReplaceDeveloperInputPort
{

    private DeveloperRepository $developerRepository;
    private ReplaceDeveloperOutputPort $outputPort;

    public function __construct(DeveloperRepository $developerRepository, ReplaceDeveloperOutputPort $outputPort)
    {
        $this->developerRepository = $developerRepository;
        $this->outputPort          = $outputPort;
    }

    public function replaceDeveloper(ReplaceDeveloperRequestModel $requestModel): ViewModel
    {
        $toBeReplaced = $requestModel->toModel();
        try {
            $this->developerRepository->findById($toBeReplaced->id) ?? throw new DeveloperNotFoundException(
                'Developer does not exists.'
            );
            return $this->outputPort->developerReplaced(
                new DeveloperResource($this->developerRepository->save($toBeReplaced))
            );
        } catch (DeveloperNotFoundException $e) {
            return $this->outputPort->developerDoesNotExists(
                new BadRequestRestResponseModel($e->getMessage(), []),
                $e
            );
        } catch (Throwable $th) {
            return $this->outputPort->unableToReplaceDeveloper(
                new UnexpectedErrorRestResponseModel(
                    'There was an unexpected error replacing developer. Please, contact the support team',
                    $toBeReplaced->toArray(),
                ),
                $th
            );
        }
    }

}