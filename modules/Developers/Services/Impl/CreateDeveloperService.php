<?php


namespace Modules\Developers\Services\Impl;


use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Presenters\CreateDeveloperOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\RequestModels\CreateDeveloperRequestModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use Modules\Developers\Services\CreateDeveloperInputPort;
use Support\Contracts\UI\ViewModel;
use Throwable;

class CreateDeveloperService implements CreateDeveloperInputPort
{

    private DeveloperRepository $developerRepository;
    private CreateDeveloperOutputPort $outputPort;

    public function __construct(DeveloperRepository $developerRepository, CreateDeveloperOutputPort $outputPort)
    {
        $this->developerRepository = $developerRepository;
        $this->outputPort          = $outputPort;
    }

    public function createDeveloper(CreateDeveloperRequestModel $requestModel): ViewModel
    {
        $toBeSaved = $requestModel->toModel();

        try {
            $saved = $this->developerRepository->save($toBeSaved);

            return $this->outputPort->developerCreated(new DeveloperResource($saved));
        } catch (Throwable $e) {
            return $this->outputPort->unableToCreateDeveloper(
                new UnexpectedErrorRestResponseModel(
                    'There was an error creating the developer. Please, contact the support team.',
                    $toBeSaved->toArray()
                ),
                $e
            );
        }
    }

}