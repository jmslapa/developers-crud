<?php


namespace Modules\Developers\Services\Impl;


use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Http\Resources\DeveloperResourceCollection;
use Modules\Developers\Presenters\ListDevelopersOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\RequestModels\ListDevelopersRequestModel;
use Support\ResponseModels\NotFoundRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use Modules\Developers\Services\ListDevelopersInputPort;
use Support\Contracts\UI\ViewModel;
use Throwable;

class ListDevelopersService implements ListDevelopersInputPort
{

    private DeveloperRepository $developerRepository;
    private ListDevelopersOutputPort $outputPort;

    public function __construct(DeveloperRepository $developerRepository, ListDevelopersOutputPort $outputPort)
    {
        $this->developerRepository = $developerRepository;
        $this->outputPort          = $outputPort;
    }

    public function listDevelopers(ListDevelopersRequestModel $requestModel): ViewModel
    {
        try {
            $list = $this->developerRepository->list($requestModel->toFiltersMap());
            if ($list->isEmpty()) {
                throw new DeveloperNotFoundException('Developers not found.');
            }

            return $this->outputPort->developersListed(new DeveloperResourceCollection($list));
        } catch (DeveloperNotFoundException $e) {
            return $this->outputPort->developersNotFound(new NotFoundRestResponseModel($e->getMessage()), $e);
        } catch (Throwable $e) {
            return $this->outputPort->unableToListDevelopers(
                new UnexpectedErrorRestResponseModel('An unexpected error has occurred'), $e
            );
        }
    }

}