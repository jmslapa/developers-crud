<?php


namespace Modules\Developers\Services;


use Support\Contracts\UI\ViewModel;

interface DeleteDeveloperInputPort
{
    public function deleteDeveloper(int $id): ViewModel;
}