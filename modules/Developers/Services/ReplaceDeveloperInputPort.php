<?php


namespace Modules\Developers\Services;


use Modules\Developers\RequestModels\ReplaceDeveloperRequestModel;
use Support\Contracts\UI\ViewModel;

interface ReplaceDeveloperInputPort
{
    public function replaceDeveloper(ReplaceDeveloperRequestModel $requestModel): ViewModel;
}