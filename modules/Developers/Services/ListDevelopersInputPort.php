<?php


namespace Modules\Developers\Services;


use Modules\Developers\RequestModels\ListDevelopersRequestModel;
use Support\Contracts\UI\ViewModel;

interface ListDevelopersInputPort
{
    public function listDevelopers(ListDevelopersRequestModel $requestModel): ViewModel;
}