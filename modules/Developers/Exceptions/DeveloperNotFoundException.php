<?php


namespace Modules\Developers\Exceptions;


use Symfony\Component\HttpKernel\Exception\HttpException;

class DeveloperNotFoundException extends HttpException
{
    public function __construct(string $message) {
        parent::__construct(404, $message);
    }

}