<?php


namespace Modules\Developers\Tests\Feature\Repositories;


use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Modules\Developers\Factories\DeveloperFactory;
use Modules\Developers\Models\Developer;
use Modules\Developers\Repositories\DeveloperRepository as DeveloperRepositoryContract;
use Support\Contracts\Domain\EloquentRepository;
use Support\Contracts\Domain\FiltersMap;
use Support\Tests\RepositoryTestCase;

/**
 * Class DeveloperRepositoryTest
 *
 * @package Modules\Developers\Tests\Feature\Repositories
 *
 * @property EloquentRepository|DeveloperRepositoryContract $repository
 * @property Factory|DeveloperFactory $factory
 */
class DeveloperRepositoryTest extends RepositoryTestCase
{

    protected function makeRepository(): EloquentRepository
    {
        return app(DeveloperRepositoryContract::class);
    }

    protected function makeFactory(): Factory
    {
        return Developer::factory();
    }

    /**
     * @param  Model|Developer  $model
     *
     * @return \Support\Contracts\Domain\FiltersMap|\Modules\Developers\FiltersMaps\DeveloperFiltersMap
     */
    protected function makeFiltersMap(Model $model = null): FiltersMap
    {
        $filters = Developer::filters();
        if ($model) {
            $filters = $model->getKey() ? $filters->id($model->id) : $filters;
            $filters->nome($model->nome)
                    ->sexo($model->sexo)
                    ->idade($model->idade)
                    ->hobby($model->hobby)
                    ->datanascimento($model->datanascimento);
        }

        return $filters;
    }

}