<?php

namespace Modules\Developers\Tests\Unit\Services;

use Exception;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Models\Developer;
use Modules\Developers\Presenters\ReplaceDeveloperOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\RequestModels\ReplaceDeveloperRequestModel;
use Modules\Developers\Services\Impl\ReplaceDeveloperService;
use PHPUnit\Framework\TestCase;
use Support\Contracts\UI\ViewModel;
use Support\ResponseModels\BadRequestRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;

class ReplaceDeveloperServiceTest extends TestCase
{

    private LegacyMockInterface|ViewModel|MockInterface $viewModelDummy;
    private LegacyMockInterface|DeveloperRepository|MockInterface $repositoryMock;
    private ReplaceDeveloperOutputPort|LegacyMockInterface|MockInterface $outputMock;
    private ReplaceDeveloperService $service;
    private LegacyMockInterface|Developer|MockInterface $developerDummy;
    private LegacyMockInterface|MockInterface|ReplaceDeveloperRequestModel $requestModelMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->viewModelDummy = Mockery::mock(ViewModel::class);
        $this->repositoryMock = Mockery::mock(DeveloperRepository::class);
        $this->outputMock     = Mockery::mock(ReplaceDeveloperOutputPort::class);
        $this->service        = new ReplaceDeveloperService($this->repositoryMock, $this->outputMock);
        $this->developerDummy = Mockery::mock(Developer::class,
            fn(MockInterface &$mock) => $mock->shouldReceive('getAttribute')
                                             ->once()
                                             ->with('id')
                                             ->andReturn(1));

        $this->requestModelMock = Mockery::mock(ReplaceDeveloperRequestModel::class,
            fn(MockInterface &$mock) => $mock->shouldReceive('toModel')
                                             ->once()
                                             ->withAnyArgs()
                                             ->andReturn($this->developerDummy));
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testShouldInvokeDeveloperDoesNotExistsWhenSuccessful()
    {
        $this->repositoryMock->shouldReceive('findById')
                             ->once()
                             ->with(Mockery::type('int'))
                             ->andReturn(null);

        $this->outputMock->shouldReceive('developerDoesNotExists')
                         ->once()
                         ->with(
                             Mockery::on(fn($arg) => $arg instanceof BadRequestRestResponseModel
                                                     && empty($arg->getErrors())),
                             Mockery::type(DeveloperNotFoundException::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->replaceDeveloper($this->requestModelMock));
    }

    public function testShouldInvokeUnableToReplaceDeveloperWhenSuccessful()
    {
        $this->developerDummy->shouldReceive('toArray')
                             ->once()
                             ->withNoArgs()
                             ->andReturn([]);

        $this->repositoryMock->shouldReceive('findById')
                             ->once()
                             ->with(Mockery::type('int'))
                             ->andReturn(Mockery::mock(Developer::class));

        $this->repositoryMock->shouldReceive('save')
                             ->once()
                             ->with($this->developerDummy)
                             ->andThrow(new Exception());

        $this->outputMock->shouldReceive('unableToReplaceDeveloper')
                         ->once()
                         ->with(
                             Mockery::type(UnexpectedErrorRestResponseModel::class),
                             Mockery::type(Exception::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->replaceDeveloper($this->requestModelMock));
    }

    public function testShouldInvokeDeveloperReplacedWhenSuccessful()
    {

        $savedDummy = Mockery::mock(Developer::class);

        $this->repositoryMock->shouldReceive('findById')
                             ->once()
                             ->with(Mockery::type('int'))
                             ->andReturn(Mockery::mock(Developer::class));

        $this->repositoryMock->shouldReceive('save')
                             ->once()
                             ->with($this->developerDummy)
                             ->andReturn($savedDummy);

        $this->outputMock->shouldReceive('developerReplaced')
                         ->once()
                         ->with(Mockery::type(DeveloperResource::class))
                         ->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->replaceDeveloper($this->requestModelMock));
    }

}
