<?php

namespace Modules\Developers\Tests\Unit\Services;

use Exception;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Models\Developer;
use Modules\Developers\Presenters\CreateDeveloperOutputPort;
use Modules\Developers\Presenters\FindDeveloperByIdOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Support\ResponseModels\NotFoundRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use Modules\Developers\Services\Impl\FindDeveloperByIdService;
use Support\Contracts\UI\ViewModel;
use Tests\TestCase;

class FindDeveloperByIdRestServiceTest extends TestCase
{

    private LegacyMockInterface|ViewModel|MockInterface $viewModelDummy;
    private LegacyMockInterface|DeveloperRepository|MockInterface $repositoryMock;
    private LegacyMockInterface|MockInterface|CreateDeveloperOutputPort $outputMock;
    private FindDeveloperByIdService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->viewModelDummy = Mockery::mock(ViewModel::class);
        $this->repositoryMock = Mockery::mock(DeveloperRepository::class);
        $this->outputMock     = Mockery::mock(FindDeveloperByIdOutputPort::class);
        $this->service        = new FindDeveloperByIdService($this->repositoryMock, $this->outputMock);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testShouldInvokeDeveloperNotFound()
    {
        $this->repositoryMock->shouldReceive('findById')
                             ->with(Mockery::type('int'))
                             ->andReturn(null);

        $this->outputMock->shouldReceive('developerNotFound')
                         ->with(
                             Mockery::type(NotFoundRestResponseModel::class),
                             Mockery::type(DeveloperNotFoundException::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->findDeveloperById(1));
    }

    public function testShouldInvokeUnableToRetrieveDeveloper()
    {
        $this->repositoryMock->shouldReceive('findById')
                             ->with(Mockery::type('int'))
                             ->andThrow(Exception::class);
        $this->outputMock->shouldReceive('unableToRetrieveDeveloper')
                         ->with(
                             Mockery::type(UnexpectedErrorRestResponseModel::class),
                             Mockery::type(Exception::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->findDeveloperById(1));
    }

    public function testShouldInvokeDeveloperRetrieved()
    {
        $developerDummy = Developer::factory()->make(['id' => 1]);
        $this->repositoryMock->shouldReceive('findById')
                             ->with(Mockery::type('int'))
                             ->andReturn($developerDummy);

        $this->outputMock->shouldReceive('developerRetrieved')
                         ->with(
                             Mockery::on(function ($arg) use ($developerDummy) {
                                 return $arg instanceof DeveloperResource
                                        && $developerDummy === $arg->resource;
                             })
                         )
                         ->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->findDeveloperById(1));
    }


}
