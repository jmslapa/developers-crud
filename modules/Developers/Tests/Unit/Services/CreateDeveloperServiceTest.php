<?php

namespace Modules\Developers\Tests\Unit\Services;

use Exception;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Models\Developer;
use Modules\Developers\Presenters\CreateDeveloperOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\RequestModels\CreateDeveloperRequestModel;
use Modules\Developers\Services\Impl\CreateDeveloperService;
use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Tests\TestCase;

class CreateDeveloperServiceTest extends TestCase
{

    private LegacyMockInterface|ViewModel|MockInterface $viewModelDummy;
    private LegacyMockInterface|DeveloperRepository|MockInterface $repositoryMock;
    private LegacyMockInterface|MockInterface|CreateDeveloperOutputPort $outputMock;
    private CreateDeveloperService $service;
    private LegacyMockInterface|Developer|MockInterface $dummyToBeSaved;
    private LegacyMockInterface|MockInterface|CreateDeveloperRequestModel $requestModelMock;

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('app.debug', false);

        $this->viewModelDummy   = Mockery::mock(ViewModel::class);
        $this->repositoryMock   = Mockery::mock(DeveloperRepository::class);
        $this->outputMock       = Mockery::mock(CreateDeveloperOutputPort::class);
        $this->service          = new CreateDeveloperService($this->repositoryMock, $this->outputMock);
        $this->dummyToBeSaved   = Mockery::mock(Developer::class,
            fn (&$mock) => $mock->shouldReceive('toArray')->andReturn([]));
        $this->requestModelMock = Mockery::mock(CreateDeveloperRequestModel::class, function (MockInterface $mock) {
            $mock->shouldReceive('toModel')->once()->withNoArgs()->andReturn($this->dummyToBeSaved);

            return $mock;
        });
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testShouldInvokeUnableToCreateDeveloperWhenSuccessful()
    {
        $this->repositoryMock->shouldReceive('save')
                             ->once()
                             ->with($this->dummyToBeSaved)
                             ->andThrow(Exception::class);

        $this->outputMock->shouldReceive('unableToCreateDeveloper')
                         ->once()
                         ->with(
                             Mockery::type(ResponseModel::class),
                             Mockery::type(Exception::class)
                         )
                         ->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->createDeveloper($this->requestModelMock));
    }

    public function testShouldInvokeDeveloperCreatedWhenSuccessful()
    {
        $savedDummy = Mockery::mock(Developer::class);
        $this->repositoryMock->shouldReceive('save')
                             ->once()
                             ->with($this->dummyToBeSaved)
                             ->andReturn($savedDummy);

        $this->outputMock->shouldReceive('developerCreated')
                         ->once()
                         ->with(Mockery::type(DeveloperResource::class))
                         ->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->createDeveloper($this->requestModelMock));
    }

}
