<?php

namespace Modules\Developers\Tests\Unit\Services;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Http\Resources\DeveloperResourceCollection;
use Modules\Developers\Models\Developer;
use Modules\Developers\Presenters\ListDevelopersOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\RequestModels\ListDevelopersRequestModel;
use Support\ResponseModels\NotFoundRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use Modules\Developers\Services\Impl\ListDevelopersService;
use Support\Contracts\Domain\FiltersMap;
use Support\Contracts\UI\ViewModel;
use Tests\TestCase;

class ListDevelopersServiceTest extends TestCase
{

    private LegacyMockInterface|FiltersMap|MockInterface $filters;
    private LegacyMockInterface|DeveloperRepository|MockInterface $repositoryMock;
    private LegacyMockInterface|MockInterface|ListDevelopersOutputPort $outputMock;
    private ListDevelopersService $service;
    private LegacyMockInterface|MockInterface|ListDevelopersRequestModel $requestModel;
    private LegacyMockInterface|ViewModel|MockInterface $viewModelDummy;

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('app.debug', false);

        $this->filters      = Mockery::mock(FiltersMap::class);
        $this->requestModel = Mockery::mock(ListDevelopersRequestModel::class, function (MockInterface $mock) {
            $mock->shouldReceive('toFiltersMap')->once()->withNoArgs()->andReturn($this->filters);

            return $mock;
        });

        $this->repositoryMock = Mockery::mock(DeveloperRepository::class);
        $this->outputMock     = Mockery::mock(ListDevelopersOutputPort::class);
        $this->service        = new ListDevelopersService($this->repositoryMock, $this->outputMock);
        $this->viewModelDummy = Mockery::mock(ViewModel::class);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testShouldInvokeDevelopersNotFoundWhenSuccess()
    {
        $this->repositoryMock->shouldReceive('list')
                             ->with($this->filters)
                             ->andReturn(Collection::empty());

        $this->outputMock->shouldReceive('developersNotFound')
                         ->with(
                             Mockery::type(NotFoundRestResponseModel::class),
                             Mockery::type(DeveloperNotFoundException::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->listDevelopers($this->requestModel));
    }

    public function testShouldInvokeUnableToListDevelopersWhenSuccess()
    {
        $this->repositoryMock->shouldReceive('list')
                             ->with($this->filters)->andThrow(Exception::class);
        $this->outputMock->shouldReceive('unableToListDevelopers')
                         ->with(
                             Mockery::type(UnexpectedErrorRestResponseModel::class),
                             Mockery::type(Exception::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->listDevelopers($this->requestModel));
    }

    public function testShouldInvokeDeveloperListedWhenSuccess()
    {
        $this->repositoryMock->shouldReceive('list')
                             ->with($this->filters)
                             ->andReturn(
                                 Developer::factory()->count(5)->make()
                             );

        $this->outputMock->shouldReceive('developersListed')
                         ->with(
                             Mockery::type(DeveloperResourceCollection::class)
                         )
                         ->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->listDevelopers($this->requestModel));
    }

}
