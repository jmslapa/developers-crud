<?php

namespace Modules\Developers\Tests\Unit\Services;

use Exception;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Models\Developer;
use Modules\Developers\Presenters\DeleteDeveloperOutputPort;
use Modules\Developers\Presenters\ReplaceDeveloperOutputPort;
use Modules\Developers\Repositories\DeveloperRepository;
use Modules\Developers\Services\Impl\DeleteDeveloperService;
use PHPUnit\Framework\TestCase;
use Support\Contracts\UI\ViewModel;
use Support\ResponseModels\BadRequestRestResponseModel;
use Support\ResponseModels\UnexpectedErrorRestResponseModel;

class DeleteDeveloperServiceTest extends TestCase
{

    private LegacyMockInterface|ViewModel|MockInterface $viewModelDummy;
    private LegacyMockInterface|DeveloperRepository|MockInterface $repositoryMock;
    private ReplaceDeveloperOutputPort|LegacyMockInterface|MockInterface $outputMock;
    private DeleteDeveloperService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->viewModelDummy = Mockery::mock(ViewModel::class);
        $this->repositoryMock = Mockery::mock(DeveloperRepository::class);
        $this->outputMock     = Mockery::mock(DeleteDeveloperOutputPort::class);
        $this->service        = new DeleteDeveloperService($this->repositoryMock, $this->outputMock);
    }

    public function testShouldDeleteDeveloperInvokeDeveloperDoesNotExistsWhenSuccessful()
    {
        $this->repositoryMock->shouldReceive('findById')
                             ->once()
                             ->with(Mockery::type('int'))
                             ->andReturn(null);

        $this->outputMock->shouldReceive('developerDoesNotExists')
                         ->once()
                         ->with(
                             Mockery::on(fn($arg) => $arg instanceof BadRequestRestResponseModel
                                                     && empty($arg->getErrors())),
                             Mockery::type(DeveloperNotFoundException::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->deleteDeveloper(1));
    }

    public function testShouldDeleteDeveloperInvokeUnableToDeleteDeveloperWhenSuccessful()
    {
        $this->repositoryMock->shouldReceive('findById')
                             ->once()
                             ->with(Mockery::type('int'))
                             ->andReturn(Mockery::mock(Developer::class));

        $this->repositoryMock->shouldReceive('delete')
                             ->once()
                             ->with(Mockery::type(Developer::class))
                             ->andThrow(new Exception());

        $this->outputMock->shouldReceive('unableToDeleteDeveloper')
                         ->once()
                         ->with(
                             Mockery::type(UnexpectedErrorRestResponseModel::class),
                             Mockery::type(Exception::class)
                         )->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->deleteDeveloper(1));
    }

    public function testShouldDeleteDeveloperInvokeDeveloperDeletedWhenSuccessful()
    {
        $developerMock = Mockery::mock(Developer::class);

        $this->repositoryMock->shouldReceive('findById')
                             ->once()
                             ->with(Mockery::type('int'))
                             ->andReturn($developerMock);

        $this->repositoryMock->shouldReceive('delete')
                             ->once()
                             ->with($developerMock);

        $this->outputMock->shouldReceive('developerDeleted')
                         ->once()
                         ->withNoArgs()
                         ->andReturn($this->viewModelDummy);

        self::assertSame($this->viewModelDummy, $this->service->deleteDeveloper(1));
    }


}
