<?php

namespace Modules\Developers\Tests\Unit\Presenters;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\MockInterface;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Presenters\Impl\CreateDeveloperRestPresenter;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Facades\ExceptionHandler;
use Tests\TestCase;

class CreateDeveloperRestPresenterTest extends TestCase
{

    private CreateDeveloperRestPresenter $presenter;
    private ResponseModel|Mockery\LegacyMockInterface|MockInterface $responseModelMock;
    private MockInterface $exceptionHandler;

    protected function setUp(): void
    {
        parent::setUp();
        parent::setUp();
        Config::set('app.debug', false);
        $this->presenter         = new CreateDeveloperRestPresenter();
        $this->responseModelMock = Mockery::mock(ResponseModel::class);
        $this->exceptionHandler  = ExceptionHandler::partialMock();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDeveloperCreatedWhenReceiveAJsonResoruce()
    {
        $jsonResponseDummy = Mockery::mock(JsonResponse::class);
        $jsonResourceMock  = Mockery::mock(DeveloperResource::class,
            function (MockInterface $mock) use ($jsonResponseDummy) {
                $mock->shouldReceive('toResponse')->once()->with(null)->andReturn($jsonResponseDummy);

                return $mock;
            });

        $viewModel = $this->presenter->developerCreated($jsonResourceMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertSame($viewModel->getResponse(), $jsonResponseDummy);
    }

    public function testDeveloperCreated()
    {
        $viewModel = $this->presenter->developerCreated($this->responseModelMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(201, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

    public function testUnableToCreateDeveloperWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->unableToCreateDeveloper($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testUnableToCreateDeveloper()
    {
        $exceptionDummy = Mockery::mock(Exception::class);
        $this->exceptionHandler->shouldReceive('report')
                               ->once()
                               ->with($exceptionDummy);
        $viewModel = $this->presenter->unableToCreateDeveloper($this->responseModelMock, $exceptionDummy);

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(500, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

}
