<?php

namespace Modules\Developers\Tests\Unit\Presenters;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Presenters\Impl\FindDeveloperByIdRestPresenter;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Facades\ExceptionHandler;
use Tests\TestCase;

class FindDeveloperByIdRestPresenterTest extends TestCase
{

    private FindDeveloperByIdRestPresenter $presenter;
    private ResponseModel|LegacyMockInterface|MockInterface $responseModelMock;
    private MockInterface $exceptionHandler;

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('app.debug', false);
        $this->presenter         = new FindDeveloperByIdRestPresenter();
        $this->responseModelMock = Mockery::mock(ResponseModel::class);
        $this->exceptionHandler  = ExceptionHandler::partialMock();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDeveloperRetrievedWhenReceiveAJsonResource()
    {
        $jsonResponseDummy = Mockery::mock(JsonResponse::class);
        $jsonResourceMock  = Mockery::mock(DeveloperResource::class,
            function (MockInterface $mock) use ($jsonResponseDummy) {
                $mock->shouldReceive('toResponse')->once()->with(null)->andReturn($jsonResponseDummy);

                return $mock;
            });

        $viewModel = $this->presenter->developerRetrieved($jsonResourceMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertSame($viewModel->getResponse(), $jsonResponseDummy);
    }

    public function testDeveloperRetrieved()
    {
        $viewModel = $this->presenter->developerRetrieved($this->responseModelMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(200, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

    public function testDeveloperNotFoundWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->developerNotFound($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testDeveloperNotFound()
    {
        $viewModel = $this->presenter->developerNotFound(
            $this->responseModelMock,
            Mockery::mock(Exception::class)
        );

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(404, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

    public function testUnableToRetrieveDeveloperWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->unableToRetrieveDeveloper($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testUnableToRetrieveDeveloper()
    {
        $exceptionDummy = Mockery::mock(Exception::class);
        $this->exceptionHandler->shouldReceive('report')
                               ->once()
                               ->with($exceptionDummy);
        $viewModel = $this->presenter->unableToRetrieveDeveloper(
            $this->responseModelMock,
            $exceptionDummy
        );

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(500, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

}
