<?php

namespace Modules\Developers\Tests\Unit\Presenters;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\MockInterface;
use Modules\Developers\Exceptions\DeveloperNotFoundException;
use Modules\Developers\Http\Resources\DeveloperResourceCollection;
use Modules\Developers\Presenters\Impl\ListDevelopersRestPresenter;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Facades\ExceptionHandler;
use Tests\TestCase;

class ListDevelopersRestPresenterTest extends TestCase
{

    private ListDevelopersRestPresenter $presenter;
    private ResponseModel|Mockery\LegacyMockInterface|MockInterface $responseModelMock;
    private MockInterface $exceptionHandler;

    public function setUp(): void
    {
        parent::setUp();
        Config::set('app.debug', false);
        $this->presenter         = new ListDevelopersRestPresenter();
        $this->responseModelMock = Mockery::mock(ResponseModel::class);
        $this->exceptionHandler = ExceptionHandler::partialMock();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDevelopersListedWhenReceiveAJsonResource()
    {
        $jsonResponseDummy = Mockery::mock(JsonResponse::class);
        $jsonResourceMock  = Mockery::mock(DeveloperResourceCollection::class,
            function (MockInterface $mock) use ($jsonResponseDummy) {
                $mock->shouldReceive('toResponse')->once()->with(null)->andReturn($jsonResponseDummy);

                return $mock;
            });

        $viewModel = $this->presenter->developersListed($jsonResourceMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertSame($viewModel->getResponse(), $jsonResponseDummy);
    }

    public function testDevelopersListed()
    {
        $viewModel = $this->presenter->developersListed($this->responseModelMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(200, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

    public function testDevelopersNotFoundWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->developersNotFound($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testDevelopersNotFound()
    {
        $viewModel = $this->presenter->developersNotFound($this->responseModelMock,
            new DeveloperNotFoundException('Test message'));

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(404, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

    public function testUnableToListDevelopersWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->unableToListDevelopers($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testUnableToListDevelopers()
    {
        $exceptionDummy = Mockery::mock(Exception::class);
        $this->exceptionHandler->shouldReceive('report')
                               ->once()
                               ->with($exceptionDummy);

        $viewModel = $this->presenter->unableToListDevelopers($this->responseModelMock, $exceptionDummy);

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(500, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

}
