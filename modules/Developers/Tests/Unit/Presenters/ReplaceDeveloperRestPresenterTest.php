<?php

namespace Modules\Developers\Tests\Unit\Presenters;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Http\Resources\DeveloperResource;
use Modules\Developers\Presenters\Impl\ReplaceDeveloperRestPresenter;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Facades\ExceptionHandler;
use Tests\TestCase;

class ReplaceDeveloperRestPresenterTest extends TestCase
{

    private ReplaceDeveloperRestPresenter $presenter;
    private ResponseModel|LegacyMockInterface|MockInterface $responseModelMock;
    private MockInterface $exceptionHandler;

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('app.debug', false);
        $this->presenter         = new ReplaceDeveloperRestPresenter();
        $this->responseModelMock = Mockery::mock(ResponseModel::class);
        $this->exceptionHandler = ExceptionHandler::partialMock();
    }

    public function testDeveloperReplacedWhenReceiveAJsonResource()
    {
        $jsonResponseDummy = Mockery::mock(JsonResponse::class);
        $jsonResourceMock  = Mockery::mock(DeveloperResource::class,
            function (MockInterface $mock) use ($jsonResponseDummy) {
                $mock->shouldReceive('toResponse')->once()->with(null)->andReturn($jsonResponseDummy);

                return $mock;
            });

        $viewModel = $this->presenter->developerReplaced($jsonResourceMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertSame($viewModel->getResponse(), $jsonResponseDummy);
    }

    public function testDeveloperReplaced()
    {
        $viewModel = $this->presenter->developerReplaced($this->responseModelMock);
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(200, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

    public function testDeveloperDoesNotExistsWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->developerDoesNotExists($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testDeveloperDoesNotExists()
    {
        $viewModel = $this->presenter->developerDoesNotExists(
            $this->responseModelMock,
            Mockery::mock(Exception::class)
        );

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(400, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }


    public function testUnableToReplaceDeveloperWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->unableToReplaceDeveloper($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testUnableToReplaceDeveloper()
    {
        $exceptionDummy = Mockery::mock(Exception::class);
        $this->exceptionHandler->shouldReceive('report')
                               ->once()
                               ->with($exceptionDummy);

        $viewModel = $this->presenter->unableToReplaceDeveloper(
            $this->responseModelMock,
            $exceptionDummy
        );

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(500, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

}
