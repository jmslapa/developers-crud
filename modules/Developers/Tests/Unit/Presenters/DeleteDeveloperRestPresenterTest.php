<?php

namespace Modules\Developers\Tests\Unit\Presenters;

use Exception;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Developers\Presenters\Impl\DeleteDeveloperRestPresenter;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Facades\ExceptionHandler;
use Tests\TestCase;

class DeleteDeveloperRestPresenterTest extends TestCase
{

    private DeleteDeveloperRestPresenter $presenter;
    private ResponseModel|LegacyMockInterface|MockInterface $responseModelMock;
    private MockInterface $exceptionHandler;

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('app.debug', false);
        $this->presenter         = new DeleteDeveloperRestPresenter();
        $this->responseModelMock = Mockery::mock(ResponseModel::class);
        $this->exceptionHandler  = ExceptionHandler::partialMock();
    }

    public function testDeveloperReplaced()
    {
        $viewModel = $this->presenter->developerDeleted();
        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(204, $viewModel->getResponse()->getStatusCode());
        self::assertEmpty($viewModel->getResponse()->getOriginalContent());
    }

    public function testDeveloperDoesNotExistsWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->developerDoesNotExists($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testDeveloperDoesNotExists()
    {
        $viewModel = $this->presenter->developerDoesNotExists(
            $this->responseModelMock,
            Mockery::mock(Exception::class)
        );

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(400, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

    public function testUnableToReplaceDeveloperWhenDebugIsActive()
    {
        Config::set('app.debug', true);
        $this->expectException(Exception::class);
        $this->presenter->unableToDeleteDeveloper($this->responseModelMock, Mockery::mock(Exception::class));
    }

    public function testUnableToReplaceDeveloper()
    {
        $exceptionDummy = Mockery::mock(Exception::class);
        $this->exceptionHandler->shouldReceive('report')
                               ->once()
                               ->with($exceptionDummy);

        $viewModel = $this->presenter->unableToDeleteDeveloper(
            $this->responseModelMock,
            $exceptionDummy
        );

        self::assertNotNull($viewModel);
        self::assertInstanceOf(RestResponseViewModel::class, $viewModel);
        self::assertEquals(500, $viewModel->getResponse()->getStatusCode());
        self::assertSame($this->responseModelMock, $viewModel->getResponse()->getOriginalContent());
    }

}
