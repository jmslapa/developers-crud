<?php

namespace Modules\Developers\Tests\Unit\RequestModels;

use Modules\Developers\Models\Developer;
use Modules\Developers\RequestModels\ReplaceDeveloperRequestModel;
use Tests\TestCase;


class ReplaceDeveloperRequestModelTest extends TestCase
{

    public function testToModel()
    {
        $model = Developer::factory()->make();
        $model->setAttribute('id', 1);
        $requestModel = new ReplaceDeveloperRequestModel($model->toArray());
        self::assertEquals($model, $requestModel->toModel());
    }

}
