<?php

namespace Modules\Developers\Tests\Unit\RequestModels;

use Modules\Developers\Models\Developer;
use Modules\Developers\RequestModels\CreateDeveloperRequestModel;
use Tests\TestCase;


class CreateDeveloperRequestModelTest extends TestCase
{

    public function testToModel()
    {
        $model = Developer::factory()->make();
        $requestModel = new CreateDeveloperRequestModel($model->toArray());
        self::assertEquals($model, $requestModel->toModel());
    }

}
