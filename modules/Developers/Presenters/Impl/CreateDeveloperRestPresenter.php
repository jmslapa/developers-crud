<?php


namespace Modules\Developers\Presenters\Impl;


use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Developers\Presenters\CreateDeveloperOutputPort;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Support\Facades\ExceptionHandler;
use Throwable;

class CreateDeveloperRestPresenter implements CreateDeveloperOutputPort
{

    public function developerCreated(ResponseModel $model): ViewModel
    {
        return new RestResponseViewModel(
            $model instanceof JsonResource
                ? $model->toResponse(null)
                : response()->json($model, 201)
        );
    }

    /**
     * @throws \Throwable
     */
    public function unableToCreateDeveloper(ResponseModel $model, Throwable $e): ViewModel
    {
        if (config('app.debug')) {
            throw  $e;
        }
        ExceptionHandler::report($e);
        return new RestResponseViewModel(
            response()->json($model, 500)
        );
    }

}