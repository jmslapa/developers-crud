<?php


namespace Modules\Developers\Presenters\Impl;


use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Developers\Presenters\ReplaceDeveloperOutputPort;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Support\Facades\ExceptionHandler;
use Throwable;

class ReplaceDeveloperRestPresenter implements ReplaceDeveloperOutputPort
{

    public function developerReplaced(ResponseModel $responseModel): ViewModel
    {
        return new RestResponseViewModel(
            $responseModel instanceof JsonResource
                ? $responseModel->toResponse(null)
                : response()->json($responseModel)
        );
    }

    /**
     * @throws \Throwable
     */
    public function developerDoesNotExists(ResponseModel $responseModel, Throwable $th): ViewModel
    {
        if (config('app.debug')) {
            throw $th;
        }

        return new RestResponseViewModel(
            response()->json($responseModel, 400)
        );
    }

    /**
     * @throws \Throwable
     */
    public function unableToReplaceDeveloper(ResponseModel $responseModel, Throwable $th): ViewModel
    {
        if (config('app.debug')) {
            throw $th;
        }
        ExceptionHandler::report($th);
        return new RestResponseViewModel(
            response()->json($responseModel, 500)
        );
    }

}