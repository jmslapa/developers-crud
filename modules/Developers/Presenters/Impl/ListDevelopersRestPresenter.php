<?php


namespace Modules\Developers\Presenters\Impl;


use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Developers\Presenters\ListDevelopersOutputPort;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Throwable;

class ListDevelopersRestPresenter implements ListDevelopersOutputPort
{

    public function developersListed(ResponseModel $responseModel): ViewModel
    {
        return new RestResponseViewModel(
            $responseModel instanceof JsonResource
                ? $responseModel->toResponse(null)
                : response()->json($responseModel, 200)
        );
    }

    /**
     * @throws \Throwable
     */
    public function developersNotFound(ResponseModel $responseModel, Throwable $th): ViewModel
    {
        if (config('app.debug')) {
            throw $th;
        }

        return new RestResponseViewModel(
            response()->json($responseModel, 404)
        );
    }

    /**
     * @throws \Throwable
     */
    public function unableToListDevelopers(ResponseModel $responseModel, Throwable $th): ViewModel {
        if (config('app.debug')) {
            throw $th;
        }
        report($th);
        return new RestResponseViewModel(
            response()->json($responseModel, 500)
        );
    }

}