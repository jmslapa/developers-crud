<?php


namespace Modules\Developers\Presenters\Impl;


use Modules\Developers\Presenters\DeleteDeveloperOutputPort;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Throwable;

class DeleteDeveloperRestPresenter implements DeleteDeveloperOutputPort
{

    public function developerDeleted(): ViewModel
    {
        return new RestResponseViewModel(
            response()->json(null, 204)
        );
    }

    /**
     * @throws \Throwable
     */
    public function developerDoesNotExists(ResponseModel $responseModel, Throwable $th): ViewModel
    {
        if (config('app.debug')) {
            throw $th;
        }

        return new RestResponseViewModel(
            response()->json($responseModel, 400)
        );
    }

    /**
     * @throws \Throwable
     */
    public function unableToDeleteDeveloper(ResponseModel $responseModel, Throwable $th): ViewModel
    {
        if (config('app.debug')) {
            throw $th;
        }
        report($th);
        return new RestResponseViewModel(
            response()->json($responseModel, 500)
        );
    }

}