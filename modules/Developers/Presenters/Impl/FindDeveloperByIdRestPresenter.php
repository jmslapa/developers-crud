<?php


namespace Modules\Developers\Presenters\Impl;


use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Developers\Presenters\FindDeveloperByIdOutputPort;
use Modules\Developers\ViewModels\RestResponseViewModel;
use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Support\Facades\ExceptionHandler;
use Throwable;

class FindDeveloperByIdRestPresenter implements FindDeveloperByIdOutputPort
{

    public function developerRetrieved(ResponseModel $responseModel): ViewModel
    {
        return new RestResponseViewModel(
            $responseModel instanceof JsonResource
                ? $responseModel->toResponse(null)
                : response()->json($responseModel)
        );
    }

    /**
     * @throws \Throwable
     */
    public function developerNotFound(ResponseModel $responseModel, Throwable $th): ViewModel
    {
        if (config('app.debug')) {
           throw $th;
        }

        return new RestResponseViewModel(
            response()->json($responseModel, 404)
        );
    }

    /**
     * @throws \Throwable
     */
    public function unableToRetrieveDeveloper(ResponseModel $responseModel, Throwable $th): ViewModel
    {
        if (config('app.debug')) {
            throw $th;
        }
        ExceptionHandler::report($th);
        return new RestResponseViewModel(
            response()->json($responseModel, 500)
        );
    }

}