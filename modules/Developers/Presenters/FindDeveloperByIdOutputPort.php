<?php


namespace Modules\Developers\Presenters;


use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Throwable;

interface FindDeveloperByIdOutputPort
{
    public function developerRetrieved(ResponseModel $responseModel): ViewModel;
    public function developerNotFound(ResponseModel $responseModel, Throwable $th): ViewModel;
    public function unableToRetrieveDeveloper(ResponseModel $responseModel, Throwable $th): ViewModel;
}