<?php


namespace Modules\Developers\Presenters;


use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Throwable;

interface ReplaceDeveloperOutputPort
{
    public function developerReplaced(ResponseModel $responseModel): ViewModel;
    public function developerDoesNotExists(ResponseModel $responseModel, Throwable $th): ViewModel;
    public function unableToReplaceDeveloper(ResponseModel $responseModel, Throwable $th): ViewModel;
}