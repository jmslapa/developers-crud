<?php


namespace Modules\Developers\Presenters;


use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Throwable;

interface ListDevelopersOutputPort
{
    public function developersListed(ResponseModel $responseModel): ViewModel;
    public function developersNotFound(ResponseModel $responseModel, Throwable $th): ViewModel;
    public function unableToListDevelopers(ResponseModel $responseModel, Throwable $th): ViewModel;
}