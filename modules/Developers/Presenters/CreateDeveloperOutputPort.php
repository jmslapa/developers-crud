<?php


namespace Modules\Developers\Presenters;


use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Throwable;

interface CreateDeveloperOutputPort
{
    public function developerCreated(ResponseModel $model): ViewModel;
    public function unableToCreateDeveloper(ResponseModel $model, Throwable $e): ViewModel;
}