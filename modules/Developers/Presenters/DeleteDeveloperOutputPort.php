<?php


namespace Modules\Developers\Presenters;


use Support\Contracts\Application\ResponseModel;
use Support\Contracts\UI\ViewModel;
use Throwable;

interface DeleteDeveloperOutputPort
{
    public function developerDeleted(): ViewModel;
    public function developerDoesNotExists(ResponseModel $responseModel, Throwable $th): ViewModel;
    public function unableToDeleteDeveloper(ResponseModel $responseModel, Throwable $th): ViewModel;
}