<?php


namespace Modules\Developers\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CreateDeveloperFormRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'nome' => ['required', 'string', 'min:3', 'max:255'],
            'sexo' => ['required', 'string', 'size:1', 'in:F,M'],
            'hobby' => ['required', 'string', 'min:3', 'max:255'],
            'datanascimento' => ['required', 'date_format:Y-m-d', 'before_or_equal: 16 years ago'],
        ];
    }
}