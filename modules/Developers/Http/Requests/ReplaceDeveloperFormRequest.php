<?php


namespace Modules\Developers\Http\Requests;


class ReplaceDeveloperFormRequest extends CreateDeveloperFormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return array_merge(
            [
                'id' => [
                    'required',
                    'int',
                    'min:0',
                    fn($attr, $val, $fail) =>
                        $val != $this->route('id')
                        ? $fail("The $attr field must be equal to the related route parameter.")
                        : null
                ]
            ],
            parent::rules()
        );
    }
}