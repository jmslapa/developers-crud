<?php


namespace Modules\Developers\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ListDevelopersFormRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id'             => ['int', 'min:0'],
            'nome'           => ['string', 'min:1', 'max:255'],
            'sexo'           => ['string', 'size:1', 'in:F,M'],
            'hobby'          => ['string', 'min:1', 'max:255'],
            'idade_inicio'   => ['int', 'min:16'],
            'idade_fim'      => ['int', $this->has('idade_inicio') ? 'gt:idade_inicio' : 'lt:999'],
            'idade'          => ['int', 'min:16', 'exclude_unless:idade_inicio,null', 'exclude_unless:idade_fim,null'],
            'datanascimento_inicio' => ['date_format:Y-m-d', 'before_or_equal: 16 years ago'],
            'datanascimento_fim' => [
                'date_format:Y-m-d',
                $this->has('datanascimento_inicio') ? 'after_or_equal:datanascimento_inicio' : 'before_or_equal:today'
            ],
            'datanascimento' => [
                'date_format:Y-m-d',
                'before_or_equal: 16 years ago',
                'exclude_unless:datanascimento_inicio,null',
                'exclude_unless:datanascimento_fim,null',
            ],
        ];
    }

}