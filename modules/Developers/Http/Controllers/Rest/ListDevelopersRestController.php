<?php

namespace Modules\Developers\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Developers\Http\Requests\ListDevelopersFormRequest;
use Modules\Developers\RequestModels\ListDevelopersRequestModel;
use Modules\Developers\Services\ListDevelopersInputPort;

class ListDevelopersRestController extends Controller
{

    private ListDevelopersInputPort $service;

    public function __construct(ListDevelopersInputPort $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Modules\Developers\Http\Requests\ListDevelopersFormRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(ListDevelopersFormRequest $request): JsonResponse
    {
        $requestModel = new ListDevelopersRequestModel($request->validated());
        return $this->service->listDevelopers($requestModel)->getResponse();
    }
}
