<?php

namespace Modules\Developers\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Developers\Services\DeleteDeveloperInputPort;

class DeleteDeveloperRestController extends \App\Http\Controllers\Controller
{

    private DeleteDeveloperInputPort $service;

    public function __construct(DeleteDeveloperInputPort $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the incoming request.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(int $id): JsonResponse
    {
        return $this->service->deleteDeveloper($id)->getResponse();
    }

}
