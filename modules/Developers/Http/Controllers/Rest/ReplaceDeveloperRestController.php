<?php

namespace Modules\Developers\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Developers\Http\Requests\ReplaceDeveloperFormRequest;
use Modules\Developers\RequestModels\ReplaceDeveloperRequestModel;
use Modules\Developers\Services\ReplaceDeveloperInputPort;

class ReplaceDeveloperRestController extends Controller
{

    private ReplaceDeveloperInputPort $service;

    public function __construct(ReplaceDeveloperInputPort $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Modules\Developers\Http\Requests\ReplaceDeveloperFormRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(ReplaceDeveloperFormRequest $request): JsonResponse
    {
        return $this->service->replaceDeveloper(new ReplaceDeveloperRequestModel($request->validated()))
                             ->getResponse();
    }
}
