<?php

namespace Modules\Developers\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Developers\Services\FindDeveloperByIdInputPort;

class FindDeveloperByIdRestController extends Controller
{

    private FindDeveloperByIdInputPort $service;

    public function __construct(FindDeveloperByIdInputPort $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the incoming request.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(int $id): JsonResponse
    {
        return $this->service->findDeveloperById($id)->getResponse();
    }

}
