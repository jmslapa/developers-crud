<?php

namespace Modules\Developers\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Developers\Http\Requests\CreateDeveloperFormRequest;
use Modules\Developers\RequestModels\CreateDeveloperRequestModel;
use Modules\Developers\Services\CreateDeveloperInputPort;

class CreateDeveloperRestController extends Controller
{

    private CreateDeveloperInputPort $service;

    public function __construct(CreateDeveloperInputPort $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Modules\Developers\Http\Requests\CreateDeveloperFormRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(CreateDeveloperFormRequest $request): JsonResponse
    {
        $requestModel = new CreateDeveloperRequestModel($request->validated());
        return $this->service->createDeveloper($requestModel)->getResponse();
    }
}
