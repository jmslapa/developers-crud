<?php

namespace Modules\Developers\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Support\Contracts\Application\ResponseModel;

class DeveloperResource extends JsonResource implements ResponseModel
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->resource->id,
            'nome'           => $this->resource->nome,
            'sexo'           => $this->resource->sexo,
            'idade'          => $this->resource->idade,
            'hobby'          => $this->resource->hobby,
            'datanascimento' => $this->resource->datanascimento,
            'updated_at'     => $this->resource->updated_at,
            'created_at'     => $this->resource->created_at,
        ];
    }

}
