<?php

namespace Modules\Developers\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Support\Contracts\Application\ResponseModel;

class DeveloperResourceCollection extends ResourceCollection implements ResponseModel
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
