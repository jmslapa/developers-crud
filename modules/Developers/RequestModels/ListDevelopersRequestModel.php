<?php


namespace Modules\Developers\RequestModels;


use Illuminate\Database\Eloquent\Model as EloquentModel;
use Modules\Developers\Models\Developer;
use Support\Contracts\Application\RequestModel;
use Support\Contracts\Domain\FiltersMap;

class ListDevelopersRequestModel
{

    private null|string $id;
    private null|string $nome;
    private null|string $sexo;
    private null|string $hobby;
    private null|string $idade_inicio;
    private null|string $idade_fim;
    private null|string $idade;
    private null|string $datanascimento_inicio;
    private null|string $datanascimento_fim;
    private null|string $datanascimento;

    public function __construct(array $attributes = [])
    {
        $this->id                    = $attributes['id'] ?? null;
        $this->nome                  = $attributes['nome'] ?? null;
        $this->sexo                  = $attributes['sexo'] ?? null;
        $this->hobby                 = $attributes['hobby'] ?? null;
        $this->idade_inicio          = $attributes['idade_inicio'] ?? null;
        $this->idade_fim             = $attributes['idade_fim'] ?? null;
        $this->idade                 = $attributes['idade'] ?? null;
        $this->datanascimento_inicio = $attributes['datanascimento_inicio'] ?? null;
        $this->datanascimento_fim    = $attributes['datanascimento_fim'] ?? null;
        $this->datanascimento        = $attributes['datanascimento'] ?? null;
    }

    public function toFiltersMap(): FiltersMap
    {
        $filters = Developer::filters();
        if ($this->id) {
            $filters->id($this->id);
        }
        if ($this->nome) {
            $filters->nome($this->nome);
        }
        if ($this->sexo) {
            $filters->sexo($this->sexo);
        }
        if ($this->hobby) {
            $filters->hobby($this->hobby);
        }
        if ($this->idade || $this->idade_inicio || $this->idade_fim) {
            $filters->idade(
                $this->idade_fim || $this->idade_inicio
                    ? [$this->idade_inicio, $this->idade_fim]
                    : $this->idade
            );
        }
        if ($this->datanascimento || $this->datanascimento_inicio || $this->datanascimento_fim) {
            $filters->datanascimento(
                $this->datanascimento_fim || $this->datanascimento_inicio
                    ? [$this->datanascimento_inicio, $this->datanascimento_fim]
                    : $this->datanascimento
            );
        }

        return $filters;
    }

    public function getId(): null|string
    {
        return $this->id;
    }

    public function setId(null|string $id): void
    {
        $this->id = $id;
    }

    public function getNome(): null|string
    {
        return $this->nome;
    }

    public function setNome(null|string $nome): void
    {
        $this->nome = $nome;
    }

    public function getSexo(): null|string
    {
        return $this->sexo;
    }

    public function setSexo(null|string $sexo): void
    {
        $this->sexo = $sexo;
    }

    public function getHobby(): null|string
    {
        return $this->hobby;
    }

    public function setHobby(null|string $hobby): void
    {
        $this->hobby = $hobby;
    }

    public function getIdade_inicio(): null|string
    {
        return $this->idade_inicio;
    }

    public function setIdade_inicio(null|string $idade_inicio): void
    {
        $this->idade_inicio = $idade_inicio;
    }

    public function getIdade_fim(): null|string
    {
        return $this->idade_fim;
    }

    public function setIdade_fim(null|string $idade_fim): void
    {
        $this->idade_fim = $idade_fim;
    }

    public function getIdade(): null|string
    {
        return $this->idade;
    }

    public function setIdade(null|string $idade): void
    {
        $this->idade = $idade;
    }

    public function getDatanascimento_inicio(): null|string
    {
        return $this->datanascimento_inicio;
    }

    public function setDatanascimento_inicio(null|string $datanascimento_inicio): void
    {
        $this->datanascimento_inicio = $datanascimento_inicio;
    }

    public function getDatanascimento_fim(): null|string
    {
        return $this->datanascimento_fim;
    }

    public function setDatanascimento_fim(null|string $datanascimento_fim): void
    {
        $this->datanascimento_fim = $datanascimento_fim;
    }

    public function getDatanascimento(): null|string
    {
        return $this->datanascimento;
    }

    public function setDatanascimento(null|string $datanascimento): void
    {
        $this->datanascimento = $datanascimento;
    }

}