<?php


namespace Modules\Developers\RequestModels;


use Support\Contracts\Application\RequestModel;

class ReplaceDeveloperRequestModel extends CreateDeveloperRequestModel implements RequestModel
{

    protected int $id;

    public function __construct($attributes = [])
    {
        $this->id = $attributes['id'];
        parent::__construct($attributes);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param  int  $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

}