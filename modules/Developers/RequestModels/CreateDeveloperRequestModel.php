<?php


namespace Modules\Developers\RequestModels;


use Illuminate\Database\Eloquent\Model as EloquentModel;
use Modules\Developers\Models\Developer;
use Support\Contracts\Application\RequestModel;

class CreateDeveloperRequestModel implements RequestModel
{

    protected string $nome;
    protected string $sexo;
    protected int $idade;
    protected string $hobby;
    protected string $datanascimento;

    public function __construct($attributes = [])
    {
        $this->nome           = $attributes['nome'];
        $this->sexo           = $attributes['sexo'];
        $this->hobby          = $attributes['hobby'];
        $this->datanascimento = $attributes['datanascimento'];
        $this->idade = now()->diffInYears($this->datanascimento);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Modules\Developers\Models\Developer
     */
    public function toModel(): EloquentModel
    {
        return Developer::factory()->make(get_object_vars($this));
    }

    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }

    /**
     * @param  string  $nome
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getSexo(): string
    {
        return $this->sexo;
    }

    /**
     * @param  string  $sexo
     */
    public function setSexo(string $sexo): void
    {
        $this->sexo = $sexo;
    }

    /**
     * @return int
     */
    public function getIdade(): int
    {
        return $this->idade;
    }

    /**
     * @param  int  $idade
     */
    public function setIdade(int $idade): void
    {
        $this->idade = $idade;
    }

    /**
     * @return string
     */
    public function getHobby(): string
    {
        return $this->hobby;
    }

    /**
     * @param  string  $hobby
     */
    public function setHobby(string $hobby): void
    {
        $this->hobby = $hobby;
    }

    /**
     * @return string
     */
    public function getDatanascimento(): string
    {
        return $this->datanascimento;
    }

    /**
     * @param  string  $datanascimento
     */
    public function setDatanascimento(string $datanascimento): void
    {
        $this->datanascimento = $datanascimento;
    }

}