<?php


namespace Modules\Developers\Repositories\Impl;


use Support\Abstracts\Domain\EloquentRepository;
use Modules\Developers\Repositories\DeveloperRepository as DeveloperRepositoryContract;

class DeveloperRepository extends EloquentRepository implements DeveloperRepositoryContract
{
}