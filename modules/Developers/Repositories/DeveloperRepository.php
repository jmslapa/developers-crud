<?php


namespace Modules\Developers\Repositories;


use Support\Contracts\Domain\EloquentRepository as EloquentRepositoryContract;

interface DeveloperRepository extends EloquentRepositoryContract
{
}