<?php


namespace Modules\Developers\Providers;


use Illuminate\Support\ServiceProvider;
use Modules\Developers\Http\Controllers\Rest\CreateDeveloperRestController;
use Modules\Developers\Http\Controllers\Rest\DeleteDeveloperRestController;
use Modules\Developers\Http\Controllers\Rest\FindDeveloperByIdRestController;
use Modules\Developers\Http\Controllers\Rest\ListDevelopersRestController;
use Modules\Developers\Http\Controllers\Rest\ReplaceDeveloperRestController;
use Modules\Developers\Models\Developer;
use Modules\Developers\Presenters\Impl\CreateDeveloperRestPresenter;
use Modules\Developers\Presenters\Impl\DeleteDeveloperRestPresenter;
use Modules\Developers\Presenters\Impl\FindDeveloperByIdRestPresenter;
use Modules\Developers\Presenters\Impl\ListDevelopersRestPresenter;
use Modules\Developers\Presenters\Impl\ReplaceDeveloperRestPresenter;
use Modules\Developers\Repositories\DeveloperRepository as DeveloperRepositoryContract;
use Modules\Developers\Repositories\Impl\DeveloperRepository;
use Modules\Developers\Services\CreateDeveloperInputPort;
use Modules\Developers\Services\DeleteDeveloperInputPort;
use Modules\Developers\Services\FindDeveloperByIdInputPort;
use Modules\Developers\Services\Impl\CreateDeveloperService;
use Modules\Developers\Services\Impl\DeleteDeveloperService;
use Modules\Developers\Services\Impl\FindDeveloperByIdService;
use Modules\Developers\Services\Impl\ListDevelopersService;
use Modules\Developers\Services\Impl\ReplaceDeveloperService;
use Modules\Developers\Services\ListDevelopersInputPort;
use Modules\Developers\Services\ReplaceDeveloperInputPort;
use Support\Abstracts\Domain\BaseModel;

class DeveloperServiceProvider extends ServiceProvider
{
    public function register()
    {
        # GLOBAL DEVELOPER REPOSITORY BIND
        $this->app->bind(DeveloperRepositoryContract::class, DeveloperRepository::class);

        # REPOSITORY DEPENDENCIES
        $this->app->when([DeveloperRepository::class])
                  ->needs(BaseModel::class)
                  ->give(Developer::class);

        # REST CONTROLLER DEPENDENCIES

        #### CREATE
        $this->app->when(CreateDeveloperRestController::class)
                  ->needs(CreateDeveloperInputPort::class)
                  ->give(fn($app) => $app->make(CreateDeveloperService::class, [
                      'outputPort' => $app->make(CreateDeveloperRestPresenter::class)
                  ]));
        #### LIST
        $this->app->when(ListDevelopersRestController::class)
                  ->needs(ListDevelopersInputPort::class)
                  ->give(fn($app) => $app->make(ListDevelopersService::class, [
                      'outputPort' => $app->make(ListDevelopersRestPresenter::class)
                  ]));

        #### FIND BY ID
        $this->app->when(FindDeveloperByIdRestController::class)
                  ->needs(FindDeveloperByIdInputPort::class)
                  ->give(fn($app) => $app->make(FindDeveloperByIdService::class, [
                      'outputPort' => $app->make(FindDeveloperByIdRestPresenter::class)
                  ]));

        #### REPLACE
        $this->app->when(ReplaceDeveloperRestController::class)
                  ->needs(ReplaceDeveloperInputPort::class)
                  ->give(fn($app) => $app->make(ReplaceDeveloperService::class, [
                      'outputPort' => $app->make(ReplaceDeveloperRestPresenter::class)
                  ]));

        #### DELETE
        $this->app->when(DeleteDeveloperRestController::class)
                  ->needs(DeleteDeveloperInputPort::class)
                  ->give(fn($app) => $app->make(DeleteDeveloperService::class, [
                      'outputPort' => $app->make(DeleteDeveloperRestPresenter::class)
                  ]));
    }

    public function boot()
    {
        //
    }

}