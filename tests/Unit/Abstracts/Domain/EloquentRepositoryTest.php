<?php

namespace Tests\Unit\Abstracts\Domain;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Support\Abstracts\Domain\BaseModel;
use Support\Abstracts\Domain\EloquentRepository;
use Support\Contracts\Domain\FiltersMap;
use Support\Database\Builder;
use Support\Traits\Tests\AvoidsProtectionDirectives;
use Tests\TestCase;

class EloquentRepositoryTest extends TestCase
{

    use AvoidsProtectionDirectives;

    protected LegacyMockInterface|MockInterface|BaseModel $modelMock;
    protected LegacyMockInterface|MockInterface|EloquentRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->modelMock  = Mockery::mock(BaseModel::class)->makePartial();
        $this->repository = $this->getMockBuilder(EloquentRepository::class)
                                 ->enableOriginalConstructor()
                                 ->setConstructorArgs([$this->modelMock])
                                 ->getMockForAbstractClass();
    }

    public function testGetModel()
    {
        $result = self::invokeInstanceMethod($this->repository, 'getModel');
        self::assertNotNull($result);
        self::assertEquals($this->modelMock, $result);
        self::assertNotSame($this->modelMock, $result);
    }

    public function testNewQuery()
    {
        $builderDummy = Mockery::mock(Builder::class);
        $this->modelMock->shouldReceive('newQuery')->once()->withNoArgs()->andReturns($builderDummy);
        $result = self::invokeInstanceMethod($this->repository, 'newQuery');
        self::assertNotNull($result);
        self::assertSame($result, $builderDummy);
    }

    public function testFilter()
    {
        $dummyResultSet = Mockery::mock(LengthAwarePaginator::class);

        $filterMock = Mockery::mock(FiltersMap::class, function (MockInterface $mock) {
            $mock->shouldReceive('apply')
                 ->with(Mockery::type(Builder::class))
                 ->andReturnArg(0);

            return $mock;
        });

        $this->modelMock->shouldReceive('scopeGetOrPaginate')
                        ->once()
                        ->andReturn($dummyResultSet);

        $result = $this->repository->list($filterMock);

        self::assertNotNull($result);
        self::assertSame($dummyResultSet, $result);
    }

    public function testFindById()
    {
        $modelDummy  = Mockery::mock(Model::class);
        $builderMock = Mockery::mock(Builder::class, function (MockInterface $mock) use ($modelDummy) {
            $mock->shouldReceive('find')
                 ->with(Mockery::type('int'))
                 ->andReturn($modelDummy);

            return $mock;
        });

        $this->modelMock->shouldReceive('newQuery')->withNoArgs()->andReturn($builderMock);

        $result = $this->repository->findById(1);

        self::assertNotNull($result);
        self::assertSame($modelDummy, $result);
    }

    public function testShouldSaveInvokeSaveMethodOfGivenModelIfItAlreadyHaveAKeyAndReturnItselfRefreshedWhenSuccessful(
    )
    {
        $arrayDummy = ['attribute' => 'Dummy attribute value'];
        $modelMock  = Mockery::mock(Model::class, function (MockInterface $mock) use ($arrayDummy) {
            $mock->shouldReceive('getKey')->atLeast(1)->withNoArgs()->andReturn(1);
            $mock->shouldReceive('toArray')->once()->withNoArgs()->andReturn($arrayDummy);

            return $mock;
        });

        $modelDummy    = Mockery::mock(Model::class);
        $persistedMock = Mockery::mock(Model::class, function (MockInterface &$mock) use ($arrayDummy, $modelDummy) {
            $mock->shouldReceive('fill')->once()->with($arrayDummy)->andReturnSelf();
            $mock->shouldReceive('save')->once()->withNoArgs();
            $mock->shouldReceive('refresh')->once()->withNoArgs()->andReturn($modelDummy);
        });

        $repository = $this->getMockBuilder(EloquentRepository::class)
                           ->enableOriginalConstructor()
                           ->setConstructorArgs([$this->modelMock])
                           ->onlyMethods(['findById'])
                           ->getMockForAbstractClass();

        $repository->expects(self::once())
                   ->method('findById')
                   ->with(self::isType('int'))
                   ->willReturn($persistedMock);


        $result = $repository->save($modelMock);

        self::assertNotNull($result);
        self::assertSame($modelDummy, $result);
    }

    public function testShouldSaveInvokeCreateMethodOfRepositoryModelPropertyIfGivenModelDoesNotHaveAnyKeyAndReturnTheCreatedModelWhenSuccessful(
    )
    {
        $arrayDummy = [];
        $modelStub  = Mockery::mock(Model::class, function (MockInterface $mock) use ($arrayDummy) {
            $mock->shouldReceive('getKey')->once()->withNoArgs()->andReturn(null);
            $mock->shouldReceive('getKeyName')->once()->withNoArgs()->andReturn('key_name');
            $mock->shouldReceive('makeHidden')->once()->with('key_name')->andReturn($mock);
            $mock->shouldReceive('toArray')->once()->withNoArgs()->andReturn($arrayDummy);

            return $mock;
        });

        $createdDummy = Mockery::mock(Model::class);

        $builderMock = Mockery::mock(Builder::class, function (MockInterface $mock) use ($arrayDummy, $createdDummy) {
            $mock->shouldReceive('create')
                 ->with($arrayDummy)
                 ->andReturn($createdDummy);

            return $mock;
        });

        $this->modelMock->shouldReceive('newQuery')->once()->withNoArgs()->andReturn($builderMock);

        $result = $this->repository->save($modelStub);

        self::assertNotNull($result);
        self::assertSame($createdDummy, $result);
    }

    public function testDelete()
    {
        $modelStub = Mockery::mock(Model::class, function (MockInterface $mock) {
            $mock->shouldReceive('delete')->once()->withNoArgs();

            return $mock;
        });

        $this->repository->delete($modelStub);
    }

}
