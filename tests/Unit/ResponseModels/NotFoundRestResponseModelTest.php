<?php

namespace Tests\Unit\ResponseModels;

use Support\ResponseModels\NotFoundRestResponseModel;
use PHPUnit\Framework\TestCase;

class NotFoundRestResponseModelTest extends TestCase
{

    private string $message;
    private \Support\ResponseModels\NotFoundRestResponseModel $responseModel;

    public function setUp(): void
    {
        parent::setUp();
        $this->message = 'Test message';
        $this->responseModel = new \Support\ResponseModels\NotFoundRestResponseModel($this->message);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testShouldBeAnArrayObject()
    {
        self::assertIsIterable($this->responseModel);
        self::assertArrayHasKey('message', $this->responseModel);
        self::assertEquals($this->message, $this->responseModel['message']);
    }
}
