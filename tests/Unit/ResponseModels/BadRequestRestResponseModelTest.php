<?php

namespace Tests\Unit\ResponseModels;

use PHPUnit\Framework\TestCase;
use Support\ResponseModels\BadRequestRestResponseModel;

class BadRequestRestResponseModelTest extends TestCase
{

    private string $message;
    private array $errors;
    private BadRequestRestResponseModel $responseModel;

    public function setUp(): void
    {
        parent::setUp();
        $this->message = 'Test message';
        $this->errors = [
            'attribute' => 'Test attribute error message'
        ];
        $this->responseModel = new \Support\ResponseModels\BadRequestRestResponseModel($this->message, $this->errors);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testShouldBeAnArrayObject()
    {
        self::assertIsIterable($this->responseModel);
        self::assertArrayHasKey('message', $this->responseModel);
        self::assertArrayHasKey('errors', $this->responseModel);
        self::assertEquals($this->message, $this->responseModel['message']);
        self::assertEquals($this->errors, $this->responseModel['errors']);
    }
}
