<?php

namespace Tests\Unit\ResponseModels;

use Support\ResponseModels\UnexpectedErrorRestResponseModel;
use PHPUnit\Framework\TestCase;

class UnexpectedErrorRestResponseModelTest extends TestCase
{

    private string $message;
    private array $modelData;
    private UnexpectedErrorRestResponseModel $responseModel;

    protected function setUp(): void
    {
        parent::setUp();
        $this->message = 'Test message';
        $this->modelData = ['attribute' => 'Test attribute'];
        $this->responseModel = new UnexpectedErrorRestResponseModel($this->message, $this->modelData);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testShouldBeAnArrayObject()
    {
        self::assertIsIterable($this->responseModel);
        self::assertArrayHasKey('message', $this->responseModel);
        self::assertArrayHasKey('data', $this->responseModel);
        self::assertEquals($this->message, $this->responseModel['message']);
        self::assertEquals($this->modelData, $this->responseModel['data']);
    }
}
