<?php


namespace Support\Database;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BaseModelQueryBuilder
 *
 * @package Support\Common
 *
 * @method Collection|LengthAwarePaginator getOrPaginate()
 */
class Builder extends EloquentBuilder
{
}