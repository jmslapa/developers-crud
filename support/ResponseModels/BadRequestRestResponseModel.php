<?php


namespace Support\ResponseModels;


use ArrayObject;
use Support\Contracts\Application\ResponseModel;

class BadRequestRestResponseModel extends ArrayObject implements ResponseModel
{

    private string $message;
    private array $errors;

    public function __construct(string $message, array $errors)
    {
        $this->message = $message;
        $this->errors  = $errors;
        parent::__construct(
            ['message' => $this->getMessage(), 'errors' => $this->getErrors()],
            ArrayObject::ARRAY_AS_PROPS
        );
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }


}