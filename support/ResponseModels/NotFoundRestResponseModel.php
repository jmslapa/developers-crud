<?php


namespace Support\ResponseModels;


use ArrayObject;
use Support\Contracts\Application\ResponseModel;

class NotFoundRestResponseModel extends ArrayObject implements ResponseModel
{
    private string $message;

    public function __construct(string $message)
    {
        parent::__construct(['message' => $message], ArrayObject::ARRAY_AS_PROPS);
        $this->message = $message;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

}