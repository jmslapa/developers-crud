<?php


namespace Support\ResponseModels;


use ArrayObject;
use JetBrains\PhpStorm\ArrayShape;
use Support\Contracts\Application\ResponseModel;

class UnexpectedErrorRestResponseModel extends ArrayObject implements ResponseModel
{
    private string $message;
    private ?array $data;

    public function __construct(string $message, array $data = null)
    {
        $this->message = $message;
        $this->data = $data;
        parent::__construct($this->buildArray(), ArrayObject::ARRAY_AS_PROPS);
    }

    #[ArrayShape(['message' => "string", 'data' => "array|null"])]
    private function buildArray(): array
    {
        $arr = ['message' => $this->message];
        if ($this->data) {
            $arr['data'] = $this->data;
        }
        return $arr;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getData(): array
    {
        return $this->data;
    }

}