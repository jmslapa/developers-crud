<?php


namespace Support\Facades;


use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerContract;
use Illuminate\Support\Facades\Facade;
use Throwable;

/**
 * Class ExceptionHandler
 *
 * @package Support\Facades
 *
 * @method static void report(Throwable $th)
 */
class ExceptionHandler extends Facade
{

    protected static function getFacadeAccessor()
    {
        return ExceptionHandlerContract::class;
    }

}