<?php


namespace Support\Traits\Tests;


use ReflectionClass;
use ReflectionException;
use RuntimeException;

trait AvoidsProtectionDirectives
{

    /**
     * @param  object  $obj
     * @param  string  $method
     * @param  array  $args
     *
     * @return mixed
     */
    public static function invokeInstanceMethod(object $obj, string $method, array $args = []): mixed
    {
        try {
            $method = (new ReflectionClass($obj))->getMethod($method);
            $method->setAccessible(true);
            return $method->invokeArgs($obj, $args);
        } catch (ReflectionException $e) {
            throw new RuntimeException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param  object  $obj
     * @param  string  $property
     * @param  mixed  $value
     *
     * @return mixed
     */
    public static function getPropertyValue(object $obj, string $property, mixed $value): mixed
    {
        try {
            $property = (new ReflectionClass($obj))->getProperty($property);
            $property->setAccessible(true);
            return $property->getValue($obj);
        } catch (ReflectionException $e) {
            throw new RuntimeException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param  object  $obj
     * @param  string  $property
     * @param  mixed  $value
     */
    public static function setPropertyValue(object $obj, string $property, mixed $value): void
    {
        try {
            $property = (new ReflectionClass($obj))->getProperty($property);
            $property->setAccessible(true);
            $property->setValue($obj, $value);
        } catch (ReflectionException $e) {
            throw new RuntimeException($e->getMessage(), 0, $e);
        }
    }
}