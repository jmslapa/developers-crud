<?php


namespace Support\Abstracts\Domain;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Collection as BaseCollection;
use Support\Database\Builder;
use Support\Contracts\Domain\EloquentRepository as EloquentRepositoryContract;
use Support\Contracts\Domain\FiltersMap;

abstract class EloquentRepository implements EloquentRepositoryContract
{

    /**
     * @var \Support\Abstracts\Domain\BaseModel
     */
    private BaseModel $model;

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
    }

    protected function getModel(): BaseModel
    {
        return clone $this->model;
    }

    protected function newQuery(): Builder|EloquentBuilder
    {
        return $this->model->newQuery();
    }


    function list(FiltersMap $filters): EloquentCollection|LengthAwarePaginator|BaseCollection
    {
        return $filters->apply($this->newQuery())->getOrPaginate();
    }

    function findById(int $id): null|EloquentModel
    {
        return $this->newQuery()->find($id);
    }

    function save(EloquentModel $model): EloquentModel
    {
        if ($model->getKey()) {
            $model = $this->findById($model->getKey())->fill($model->toArray());
            $model->save();
            $model = $model->refresh();
        } else {
            $model = $this->newQuery()->create($model->makeHidden($model->getKeyName())->toArray());
        }
        return $model;
    }

    function delete(EloquentModel $model): void
    {
        $model->delete();
    }

}