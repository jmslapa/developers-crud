<?php


namespace Support\Abstracts\Domain;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use Support\Database\Builder;
use TypeError;

/**
 * Class BaseModel
 *
 * @package Support\Abstracts\Domain
 */
class BaseModel extends Model
{

    public function scopeGetOrPaginate(EloquentBuilder $query): Collection|LengthAwarePaginator|array
    {
        $args = Request::only(['page', 'per_page']);
        $page = $args['page'] ?? null;
        $perPage = $args['per_page'] ?? null;
        if($page) {
            return $query->paginate($perPage && $perPage > 0 ? $perPage : $this->perPage);
        }
        return $query->get();
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     *
     * @return \Support\Database\Builder
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

}