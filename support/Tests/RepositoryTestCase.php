<?php


namespace Support\Tests;


use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Support\Contracts\Domain\EloquentRepository;
use Support\Contracts\Domain\FiltersMap;
use Tests\TestCase;

abstract class RepositoryTestCase extends TestCase
{
    use RefreshDatabase;

    protected EloquentRepository $repository;
    protected Factory $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->makeRepository();
        $this->factory    = $this->makeFactory();
    }

    protected abstract function makeRepository(): EloquentRepository;

    protected abstract function makeFactory(): Factory;

    protected abstract function makeFiltersMap(Model $model = null): FiltersMap;

    public function testListWithoutFiltersShouldReturnANonEmptyListWhenSuccessful()
    {
        $persisted = $this->factory->create();
        $list      = $this->repository->list($this->makeFiltersMap());
        self::assertNotNull($list);
        self::assertNotEmpty($list);
        self::assertContainsOnlyInstancesOf($persisted::class, $list);
        self::assertEquals(1, $list->filter(fn(Model $e) => $e->is($persisted))->count());
    }

    public function testListWithFiltersShouldReturnANonEmptyListWhenSuccessful()
    {
        $persisted = $this->factory->create();
        $filters   = $this->makeFiltersMap($persisted);

        $list = $this->repository->list($filters);

        self::assertNotNull($list);
        self::assertNotEmpty($list);
        self::assertContainsOnlyInstancesOf($persisted::class, $list);
        self::assertEquals(1, $list->filter(fn(Model $e) => $e->is($persisted))->count());
    }

    public function testListWithFiltersShouldReturnAnEmptyListWhenSuccessful()
    {
        $this->factory->count(5)->create();
        $wrongFilters = $this->makeFiltersMap($this->factory->make());

        $list2 = $this->repository->list($wrongFilters);

        self::assertNotNull($list2);
        self::assertEmpty($list2);
    }

    public function testFindByIdShouldReturnAModelWhenSuccessful()
    {
        $persisted = $this->factory->create();
        $result    = $this->repository->findById($persisted->getKey());

        self::assertNotNull($result);
        self::assertEquals($persisted->getAttributes(), $result->getAttributes());
    }

    public function testSaveShouldCreateANewRegister()
    {
        $toPersist = $this->factory->make();
        $persisted = $this->repository->save($toPersist);
        $result = $this->repository->findById($persisted->getKey());

        self::assertNotNull($persisted);
        self::assertEquals(
            $persisted->only($persisted->getFillable()),
            $toPersist->only($toPersist->getFillable())
        );
        self::assertNotNull($result);
        self::assertEquals($persisted->getAttributes(), $result->getAttributes());
    }

    public function testSaveShouldReplaceAnExistingRegister()
    {
        $persisted = $this->factory->create();
        $toReplace = $this->repository->findById($persisted->getKey());
        $replaced = $this->repository->save($toReplace);

        self::assertNotNull($replaced);
        self::assertEquals($toReplace->only($toReplace->getFillable()), $replaced->only($toReplace->getFillable()));
    }

    public function testDeleteShouldDeleteAnExistingRegister()
    {
        $persisted   = $this->factory->create();
        $this->repository->delete($persisted);

        self::assertNull($this->repository->findById($persisted->getKey()));
    }
}