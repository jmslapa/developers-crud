<?php


namespace Support\Contracts\Domain;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Collection as BaseCollection;

interface EloquentRepository
{
    function list(FiltersMap $filters): EloquentCollection|LengthAwarePaginator|BaseCollection;

    function findById(int $id): null|EloquentModel;

    function save(EloquentModel $model): EloquentModel;

    function delete(EloquentModel $model): void;
}