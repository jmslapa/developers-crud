<?php


namespace Support\Contracts\Domain;


use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Support\Database\Builder as CustomBuilder;

interface FiltersMap
{
    function apply(QueryBuilder|EloquentBuilder $query): QueryBuilder|EloquentBuilder|CustomBuilder;
}