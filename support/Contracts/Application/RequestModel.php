<?php


namespace Support\Contracts\Application;


use Illuminate\Database\Eloquent\Model as EloquentModel;

interface RequestModel
{
    function toModel(): EloquentModel;
}