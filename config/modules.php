<?php

return [
    'directories' => [
        'Http' => [
            'Controllers'
        ],
        'Migrations',
        'Models',
        'Providers',
        'Repositories',
        'Services',
        'Routes',
        'Tests' => [
            'Feature',
            'Unit'
        ],
    ]
];
